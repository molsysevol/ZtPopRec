# Comparison of LD maps with CO maps
# Created on February 5th, 2016
# by Julien Y. Dutheil and Eva H. Stukenbrock

# CO data provided by Daniel Croll, published in
#
# Croll D1, Lendenmann MH2, Stewart E2, McDonald BA2.
# The Impact of Recombination Hotspots on Genome Evolution of a Fungal Plant Pathogen.
# Genetics. 2015 Nov;201(3):1213-28. doi: 10.1534/genetics.115.180968. Epub 2015 Sep 21.

###                     ###
### Compare all LD maps ###
###                     ###

interval00500 <- read.table("ZtRecRate_interval_t0.05.txt.gz", header = TRUE, stringsAsFactors = FALSE)
interval00050 <- read.table("ZtRecRate_interval_t0.005.txt.gz", header = TRUE, stringsAsFactors = FALSE)
interval00005 <- read.table("ZtRecRate_interval_t0.0005.txt.gz", header = TRUE, stringsAsFactors = FALSE)

ldhelmet00500 <- read.table("ZtRecRate_ldhelmet_t0.05.txt.gz", header = TRUE, stringsAsFactors = FALSE)
ldhelmet00050 <- read.table("ZtRecRate_ldhelmet_t0.005.txt.gz", header = TRUE, stringsAsFactors = FALSE)
ldhelmet00005 <- read.table("ZtRecRate_ldhelmet_t0.0005.txt.gz", header = TRUE, stringsAsFactors = FALSE)

# Merge all maps (they are based on the same SNPs):
interval00500$IntervalMeanRhoTheta0.05<-interval00500$Mean_rho
interval00050$IntervalMeanRhoTheta0.005<-interval00050$Mean_rho
interval00005$IntervalMeanRhoTheta0.0005<-interval00005$Mean_rho
rec.interval <- merge(interval00500[,c(1,6,7)], interval00050[,c(1,6,7)], by = c("Loci", "Chr"), all = TRUE)
rec.interval <- merge(rec.interval, interval00005[,c(1,6,7)], by = c("Loci", "Chr"), all = TRUE)

ldhelmet00500$LDhelmetMeanRhoTheta0.05<-ldhelmet00500$Mean_rho
ldhelmet00050$LDhelmetMeanRhoTheta0.005<-ldhelmet00050$Mean_rho
ldhelmet00005$LDhelmetMeanRhoTheta0.0005<-ldhelmet00005$Mean_rho
rec.ldhelmet <- merge(ldhelmet00500[,c(1,2,7)], ldhelmet00050[,c(1,2,7)], by = c("Loci", "Chr"), all = TRUE)
rec.ldhelmet <- merge(rec.ldhelmet, ldhelmet00005[,c(1,2,7)], by = c("Loci", "Chr"), all = TRUE)

rec <- merge(rec.interval, rec.ldhelmet, by = c("Loci", "Chr"), all = FALSE)

# PCA:
require(ade4)
dat <- rec[, 3:8]
names(dat) <- c("LDhat 0.05", "LDhat 0.005", "LDhat 0.0005", "LDhelmet 0.05", "LDhelmet 0.005", "LDhelmet 0.0005")
dudi <- dudi.pca(dat, nf = 2, scannf = FALSE)
s.corcircle(dudi$co)

dev.print(pdf, file = "LDMethodsCorCircle.pdf", width = 9, height = 6)

# Make a ggplot figure (nicer):
library(ggplot2)
library(ggrepel)

plot.loadings <- function(loads, x, y) {
  pdat <- data.frame(Name = row.names(loads), PC1 = loads[, x], PC2 = loads[, y])
  p <- ggplot(pdat, aes(x = PC1, y = PC2))
  p <- p + geom_vline(xintercept = 0)
  p <- p + geom_hline(yintercept = 0)
  p <- p + annotate("path",
                    x=cos(seq(0,2*pi,length.out=100)),
                    y=sin(seq(0,2*pi,length.out=100)))
  #p <- p + geom_segment(x = 0, y = 0, aes(xend = PC1, yend = PC2), arrow = arrow(length = unit(0.3, "cm")))
  p <- p + geom_point()
  p <- p + geom_label_repel(aes(label = Name), fill = "black", col = "white", alpha = 1, segment.color = "black")
  p <- p + coord_fixed()
  return(p)
}

p.pca <- plot.loadings(dudi$co, 1, 2) + xlab("PC1") + ylab("PC2")


# Filter per genomic region:
anno.zt <- read.table("ZtAnnotations.txt.gz", header = TRUE, stringsAsFactors = FALSE)
anno.zt <- subset(anno.zt, !is.na(start) & !is.na(end))

feature2map <- function(rec, gff, features) {
  rec.features<-NULL
  for (chr in unique(gff$seqname)) {
    gff.chr<-subset(gff, seqname == chr & feature %in% features)
    rec.chr<-subset(rec, Chr == chr)
    cat("\n", chr, nrow(gff.chr), "features and", nrow(rec.chr), "SNPs\n")
    if (nrow(rec.chr) > 0) {
      rec.chr[, features] <- FALSE
      for (f in features) {
        cat("\nFeature: ", f, "\n")
        gff.chr.feature <- subset(gff.chr, feature == f)
        if (nrow(gff.chr.feature) > 0) {
          bp<-txtProgressBar(0, nrow(gff.chr.feature), style = 3)
          for (i in 1:nrow(gff.chr.feature)) {
            setTxtProgressBar(bp, i)
            rec.chr[rec.chr$Loci >= gff.chr.feature[i, "start"] & rec.chr$Loci < gff.chr.feature[i, "end"], f]<-TRUE
          }
        }
      }
      rec.features<-rbind(rec.features, rec.chr)
    }
  }
  return(rec.features)
}

rec.feat <- feature2map(rec, anno.zt, c("intron", "exon", "intergenic500", "UTR3p.500", "UTR5p.500"))

require(ade4)
dat <- rec.feat[rec.feat$intergenic500, 3:8]
names(dat) <- c("LDhat 0.05", "LDhat 0.005", "LDhat 0.0005", "LDhelmet 0.05", "LDhelmet 0.005", "LDhelmet 0.0005")
dudi <- dudi.pca(dat, nf = 2, scannf = FALSE)
s.corcircle(dudi$co)

dat <- rec.feat[rec.feat$exon, 3:8]
names(dat) <- c("LDhat 0.05", "LDhat 0.005", "LDhat 0.0005", "LDhelmet 0.05", "LDhelmet 0.005", "LDhelmet 0.0005")
dudi <- dudi.pca(dat, nf = 2, scannf = FALSE)
s.corcircle(dudi$co)

# => Very similar results


###                      ###
### Compare with CO maps ###
###                      ###

map.co <- read.table("DanielCroll_July2015_Two_crosses.cM_per_20kb.txt.gz",
                     header = TRUE, stringsAsFactors = FALSE)

# Recombination rate was averaged in windows of 20 kb. We compute the corresponding averages
# for LD maps, for both LDhat and LDhelmet.
# We first reorganize the data to compare estimates from the two crosses:

require(reshape2)
map.co<-dcast(map.co, chr + start + end ~ cross, value.var = "cM_per_20kb")

# Then we sort the windows according to coordinates:
map.co <- map.co[order(map.co$chr, map.co$start),]

require(plyr)

read.rec <- function(file) {
  map <- read.table(file, header = TRUE, stringsAsFactors = FALSE)
  fx <- function(map) {
    map$Loci2 <- c(map$Loci[-1], NA)
    return(map[-nrow(map),])
  }
  return(ddply(map, .variables = "Chr", .fun = fx))
}

map.ldhat <- read.rec("ZtRecRate_interval_t0.005.txt.gz")
map.ldhat8seq <- read.rec("Zt8SeqRecRate_interval_t0.01.txt.gz")
map.ldhatNoSing <- read.rec("ZtRecRate_maf20_interval_t0.005.txt.gz")
map.ldhelmet <- read.rec("ZtRecRate_ldhelmet_t0.005.txt.gz")

# Filter according to confidence interval
map.ldhat.f <- subset(map.ldhat, (U95-L95) / Mean_rho < 2)
100 * nrow(map.ldhat.f) / nrow(map.ldhat) # 51%
map.ldhat8seq.f <- subset(map.ldhat8seq, (U95-L95) / Mean_rho < 2)
100 * nrow(map.ldhat8seq.f) / nrow(map.ldhat8seq) # 50%
map.ldhatNoSing.f <- subset(map.ldhatNoSing, (U95-L95) / Mean_rho < 2)
100 * nrow(map.ldhatNoSing.f) / nrow(map.ldhatNoSing) # 72% => Most of the filtered SNPs were actually singletons!
map.ldhelmet.f <- subset(map.ldhelmet, (U95-L95) / Mean_rho < 2)
100 * nrow(map.ldhelmet.f) / nrow(map.ldhelmet) # 79%
# => LDhelmet provides narrower confidence intervals

# Filter according to genetic diversity:

compute.density <- function(rec, wsize, n = 13, varname = "Diversity") {
  cat("Scanning chr", unique(rec$Chr), "...\n")
  an <- sum(1/(1:(n-1)))
  div <- numeric(nrow(rec))
  pb <- txtProgressBar(1, nrow(rec), style = 3)
  for (i in 1:nrow(rec)) {
    setTxtProgressBar(pb, i)
    x <- rec[i, "Loci"]
    K <- sum(rec$Loci > x - wsize / 2 & rec$Loci <= x + wsize / 2)
    div[i] <- (K / an) / wsize # Note: this underestimate the diversity for the border regions (start and end of chromosome), but this is fine as we most likely want to discard those anyway
  }
  rec[, varname] <- div
  return(rec)
}

map.ldhat <- ddply(.data = map.ldhat, .variables = "Chr", .fun = compute.density, wsize = 2000, varname = "Div2kb")
hist(map.ldhat$Div2kb)
summary(map.ldhat$Div2kb)
sum(map.ldhat$Div2kb < 0.001)
100*sum(map.ldhat$Div2kb < 0.001) / nrow(map.ldhat)

map.ldhat.f2 <- subset(map.ldhat, (U95-L95) / Mean_rho < 2 & Div2kb >= 0.001)
100 * nrow(map.ldhat.f2) / nrow(map.ldhat) # 50% Basically does not change much, filtering on confidence interval is more efficient.


# This function computes averages:
add.ld <- function(map.co, map.ld, name, name2 = NULL) {
  pb <- txtProgressBar(0, nrow(map.co), style = 3)
  tmp <- NULL
  currentChr <- 0
  for (i in 1:nrow(map.co)) {
    setTxtProgressBar(pb, i)
    chr <- map.co[i, "chr"]
    if (chr != currentChr) {
      tmp <- subset(map.ld, Chr == chr)
      currentChr <- chr
    }
    rec <- subset(tmp, Loci >= map.co[i, "start"] & Loci < map.co[i, "end"])
    if (nrow(rec) > 0) {
      map.co[i, name] <- weighted.mean(rec$Mean_rho, rec$Loci2 - rec$Loci)
    }
    else {
      map.co[i, name] <- NA
    }
    if (!is.null(name2))
      map.co[i, name2] <- nrow(rec)
  }
  return(map.co)
}

# We run it on the two LD maps:
map.co <- add.ld(map.co, map.ldhat, "LDhat")
map.co <- add.ld(map.co, map.ldhat8seq, "LDhat8seq")
map.co <- add.ld(map.co, map.ldhatNoSing, "LDhatNoSingleton")
map.co <- add.ld(map.co, map.ldhelmet, "LDhelmet")

# And on the filtered maps:
map.co.f <- add.ld(map.co, map.ldhat.f, "LDhat")
map.co.f <- add.ld(map.co.f, map.ldhat8seq.f, "LDhat8seq")
map.co.f <- add.ld(map.co.f, map.ldhatNoSing.f, "LDhatNoSingleton")
map.co.f <- add.ld(map.co.f, map.ldhelmet.f, "LDhelmet")

# Only intergenic or non-coding regions:
map.ldhat.feat <- feature2map(map.ldhat, anno.zt, c("exon", "intergenic500"))
map.ldhelmet.feat <- feature2map(map.ldhelmet, anno.zt, c("exon", "intergenic500"))
map.ldhat.f.feat <- feature2map(map.ldhat.f, anno.zt, c("exon", "intergenic500"))
map.ldhelmet.f.feat <- feature2map(map.ldhelmet.f, anno.zt, c("exon", "intergenic500"))

map.co <- add.ld(map.co, subset(map.ldhat.feat, map.ldhat.feat$intergenic500), "LDhatIntergenic500")
map.co <- add.ld(map.co, subset(map.ldhelmet.feat, map.ldhelmet.feat$intergenic500), "LDhelmetIntergenic500")
map.co <- add.ld(map.co, subset(map.ldhat.feat, !map.ldhat.feat$exon), "LDhatNonCoding")
map.co <- add.ld(map.co, subset(map.ldhelmet.feat, !map.ldhelmet.feat$exon), "LDhelmetNonCoding")

#Only for looking at correlations between filtered and non-filtered maps.
#map.co <- add.ld(map.co, subset(map.ldhat.f.feat, map.ldhat.f.feat$intergenic500), "LDhatFilteredIntergenic500")
#map.co <- add.ld(map.co, subset(map.ldhelmet.f.feat, map.ldhelmet.f.feat$intergenic500), "LDhelmetFilteredIntergenic500")
#map.co <- add.ld(map.co, subset(map.ldhat.f.feat, !map.ldhat.f.feat$exon), "LDhatFilteredNonCoding")
#map.co <- add.ld(map.co, subset(map.ldhelmet.f.feat, !map.ldhelmet.f.feat$exon), "LDhelmetFilteredNonCoding")

map.co.f <- add.ld(map.co.f, subset(map.ldhat.f.feat, map.ldhat.f.feat$intergenic500), "LDhatIntergenic500")
map.co.f <- add.ld(map.co.f, subset(map.ldhelmet.f.feat, map.ldhelmet.f.feat$intergenic500), "LDhelmetIntergenic500")
map.co.f <- add.ld(map.co.f, subset(map.ldhat.f.feat, !map.ldhat.f.feat$exon), "LDhatNonCoding")
map.co.f <- add.ld(map.co.f, subset(map.ldhelmet.f.feat, !map.ldhelmet.f.feat$exon), "LDhelmetNonCoding")


# Look at correlations between maps:
cor.test(~map.co[,"SW5xSW39"]+map.co[,"3D7x3D1"], method = "kendall")
# tau = 0.430 ***

### LD maps:
cor.test(~map.co[,"LDhat"]+map.co[,"3D7x3D1"], method = "kendall")
# tau = 0.2709597 ***

cor.test(~map.co[,"LDhelmet"]+map.co[,"3D7x3D1"], method = "kendall")
# tau = 0.2393105 ***

cor.test(~map.co[,"LDhat"]+map.co[,"SW5xSW39"], method = "kendall")
# tau = 0.2316428 ***

cor.test(~map.co[,"LDhelmet"]+map.co[,"SW5xSW39"], method = "kendall")
# tau = 0.1985764 ***

cor.test(~map.co.f[,"LDhat"]+map.co.f[,"3D7x3D1"], method = "kendall")
# tau = 0.3055936 ***

cor.test(~map.co.f[,"LDhelmet"]+map.co.f[,"3D7x3D1"], method = "kendall")
# tau = 0.2643591 ***

cor.test(~map.co.f[,"LDhat"]+map.co.f[,"SW5xSW39"], method = "kendall")
# tau = 0.2823516 ***

cor.test(~map.co.f[,"LDhelmet"]+map.co.f[,"SW5xSW39"], method = "kendall")
# tau = 0.221793 ***


###
### Correlations are lower with the second cross, and are better with filtered map.
### LDhat also give higher correlations than LDhelmet.
###

# We compute the average between the two crosses:
map.co$AveCrosses <- (map.co[,"3D7x3D1"] + map.co[,"SW5xSW39"]) / 2
map.co.f$AveCrosses <- (map.co.f[,"3D7x3D1"] + map.co.f[,"SW5xSW39"]) / 2
summary(map.co.f$AveCrosses * 1e6 / 20e3)
# Median 28.29, mean 66.07 cM / Mb

# Write file for reuse in Grandaubert, Dutheil and Stukenbrock:
write.csv(map.co.f, "Croll+LDhat_20kbMap.csv")

cor.test(~LDhat+AveCrosses, map.co, method = "kendall")
# tau = 0.2869266 ***
cor.test(~LDhat+AveCrosses, map.co.f, method = "kendall")
# tau = 0.3405613 ***

cor.test(~LDhelmet+AveCrosses, map.co, method = "kendall")
# tau = 0.2495309 ***
cor.test(~LDhelmet+AveCrosses, map.co.f, method = "kendall")
# tau = 0.2778863 ***

cor.test(~LDhat8seq+AveCrosses, map.co, method = "kendall")
# tau = 0.2642362 ***
cor.test(~LDhat8seq+AveCrosses, map.co.f, method = "kendall")
# tau = 0.3237214 ***

cor.test(~LDhatNoSingleton+AveCrosses, map.co, method = "kendall")
# tau = 0.213601 ***
cor.test(~LDhatNoSingleton+AveCrosses, map.co.f, method = "kendall")
# tau = 0.3082899 ***

# (all p-values < 2.2e-16)
# Better correlation when taking the average of the two crosses.
# Better correlation with LDhat than LDhelmet, and when taking all sequences into account, as well as singletons.

# Intergenic:
cor.test(~LDhatIntergenic500+AveCrosses, map.co, method = "kendall")
# 0.2185717
cor.test(~LDhelmetIntergenic500+AveCrosses, map.co, method = "kendall")
# 0.2336972
cor.test(~LDhatIntergenic500+AveCrosses, map.co.f, method = "kendall")
# 0.2208062
cor.test(~LDhelmetIntergenic500+AveCrosses, map.co.f, method = "kendall")
# 0.2356458

# Non-coding:
cor.test(~LDhatNonCoding+AveCrosses, map.co, method = "kendall")
# 0.2513422
cor.test(~LDhelmetNonCoding+AveCrosses, map.co, method = "kendall")
# 0.242379
cor.test(~LDhatNonCoding+AveCrosses, map.co.f, method = "kendall")
# 0.2990208
cor.test(~LDhelmetNonCoding+AveCrosses, map.co.f, method = "kendall")
# 0.2670892

map.co$AveMethods <- (map.co[,"LDhat"] + map.co[,"LDhelmet"]) / 2
map.co.f$AveMethods <- (map.co.f[,"LDhat"] + map.co.f[,"LDhelmet"]) / 2

cor.test(~AveMethods+AveCrosses, map.co, method = "kendall")
# 0.2737899 *** (< 2.2e-16)
cor.test(~AveMethods+AveCrosses, map.co.f, method = "kendall")
# 0.3102612 *** (<2.2e-16)
# Does not improve compared with LDhat only.


# Figures:

require(reshape2)
map.co2 <- melt(data = map.co.f, id.vars = c("chr", "start", "end", "3D7x3D1", "SW5xSW39", "AveCrosses"), variable.name = "Method", value.name = "LDrate")
map.co2 <- subset(map.co2, Method %in% c("LDhat", "LDhelmet"))
require(ggplot2)
require(scales)
p <- ggplot(data=map.co2, aes(x = AveCrosses, y = LDrate))
p <- p + geom_point(aes(col = Method), alpha = 0.5)
p <- p + scale_x_log10() + scale_y_log10()
p <- p + theme_bw() + scale_color_brewer(palette = "Set1") + theme(legend.position = "top")
p <- p + xlab("Average rate from crosses") + ylab("rho = 4.Ne.r")
p <- p + stat_smooth(method="lm", aes(col = Method), se = FALSE)
p

# Use discretization for better readability:
library(Hmisc)
map.co.f2 <- map.co.f
map.co.f2$AveCrossesF <- cut2(map.co.f2$AveCrosses, g = 10)

map.co.g <- ddply(.data = map.co.f2, .variables = "AveCrossesF", .fun = plyr::summarize,
      MeanCO = mean(AveCrosses, na.rm = TRUE),
      MedianCO = median(AveCrosses, na.rm = TRUE),
      MeanLDhat = mean(LDhat, na.rm = TRUE),
      StdErrLDhat = sd(LDhat, na.rm = TRUE) / sqrt(sum(!is.na(LDhat))),
      MedianLDhat = median(LDhat, na.rm = TRUE),
      LowLDhat = quantile(LDhat, prob = 0.25, na.rm = TRUE),
      HighLDhat = quantile(LDhat, prob = 0.75, na.rm = TRUE),
      MeanLDhelmet = mean(LDhelmet, na.rm = TRUE),
      StdErrLDhelmet = sd(LDhelmet, na.rm = TRUE) / sqrt(sum(!is.na(LDhelmet))),
      MedianLDhelmet = median(LDhelmet, na.rm = TRUE),
      LowLDhelmet = quantile(LDhelmet, prob = 0.25, na.rm = TRUE),
      HighLDhelmet = quantile(LDhelmet, prob = 0.75, na.rm = TRUE))

map.co.g$AveCrossesF <- NULL

# Plot LDhat vs LDhelmet:
p <- ggplot(data=map.co.g, aes(x = MeanLDhat, y = MeanLDhelmet))
p <- p + geom_point()
p <- p + geom_errorbar(aes(ymin = MeanLDhelmet - StdErrLDhelmet,
                           ymax = MeanLDhelmet + StdErrLDhelmet))
p <- p + geom_errorbarh(aes(xmin = MeanLDhat - StdErrLDhat,
                           xmax = MeanLDhat + StdErrLDhat))
p <- p + geom_abline(intercept = 0, slope = 1)
p <- p + theme_bw() + coord_fixed() + ylim(0, 0.08) + xlim(0, 0.08)
p <- p + xlab("LDhat") + ylab("LDhelmet")
p

# The same but according to LDhat, and not CO:
map.co.f2 <- map.co.f
map.co.f2$LDhatF <- cut2(map.co.f2$LDhat, g = 10)

map.co.g2 <- ddply(.data = map.co.f2, .variables = "LDhatF", .fun = plyr::summarize,
                  MeanLDhat = mean(LDhat, na.rm = TRUE),
                  MedianLDhat = median(LDhat, na.rm = TRUE),
                  MeanLDhelmet = mean(LDhelmet, na.rm = TRUE),
                  StdErrLDhelmet = sd(LDhelmet, na.rm = TRUE) / sqrt(sum(!is.na(LDhelmet))),
                  MedianLDhelmet = median(LDhelmet, na.rm = TRUE),
                  LowLDhelmet = quantile(LDhelmet, prob = 0.25, na.rm = TRUE),
                  HighLDhelmet = quantile(LDhelmet, prob = 0.75, na.rm = TRUE))

map.co.g2$LDhatF <- NULL

p <- ggplot(data=map.co.g2, aes(x = MeanLDhat, y = MeanLDhelmet))
p <- p + geom_point(col = "cornflowerblue")
p <- p + geom_errorbar(aes(ymin = MeanLDhelmet - StdErrLDhelmet,
                           ymax = MeanLDhelmet + StdErrLDhelmet),
                       col = "cornflowerblue")
p <- p + geom_abline(intercept = 0, slope = 1)
p <- p + theme_bw() + coord_fixed() + ylim(0, 0.08) + xlim(0, 0.06)
p <- p + xlab("LDhat") + ylab("LDhelmet")
p

# Mean might be biased, look at median and quartiles:
p <- ggplot(data=map.co.g2, aes(x = MedianLDhat, y = MedianLDhelmet))
p <- p + geom_point(col = "cornflowerblue")
p <- p + geom_errorbar(aes(ymin = LowLDhelmet,
                           ymax = HighLDhelmet),
                       col = "cornflowerblue")
p <- p + geom_abline(intercept = 0, slope = 1)
p <- p + theme_bw() + coord_fixed()
p <- p + xlab("LDhat") + ylab("LDhelmet")
p


p.hathel <- p

library(data.table)
map.co.g3 <- melt.data.table(data = as.data.table(map.co.g),
                             id.vars = c("MedianCO"),
                             measure.vars = patterns("MedianLD*", "LowLD*", "HighLD*"),
                             variable.name = "Method", value.name = c("Median", "Low", "High"))
map.co.g3$Method <- c("LDhat", "LDhelmet")[map.co.g3$Method]
map.co.g3 <- na.omit(map.co.g3)
map.co.g3$X[map.co.g3$Method == "LDhat"] <- (0.02 - sqrt(map.co.g3$MedianCO[map.co.g3$Method == "LDhat"]))^2
map.co.g3$X[map.co.g3$Method == "LDhelmet"] <- (0.02 + sqrt(map.co.g3$MedianCO[map.co.g3$Method == "LDhelmet"]))^2
map.co.g3$X[map.co.g3$Method == "LDhat" & map.co.g3$Median == 0.0] <- -0.02
map.co.g3$X[map.co.g3$Method == "LDhelmet" & map.co.g3$Median == 0.0] <- 0.02

p <- ggplot(data=map.co.g3, aes(x = MedianCO, y = Median))
p <- p + geom_point(aes(col = Method))
p <- p + geom_errorbar(aes(x = X, ymin = Low, ymax = High, col = Method))
p <- p + scale_x_sqrt(breaks = c(0.1, 0.5, 1, 2, 3, 6), minor_breaks = NULL)
p <- p + scale_y_sqrt(breaks = c(0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08), minor_breaks = NULL)
p <- p + theme_bw() + scale_color_brewer(palette = "Set1") + theme(legend.position = "top")
p <- p + xlab("Average rate from crosses") + ylab(expression(rho = 2*N[e]*r))
p <- p + geom_quantile(aes(col = Method), quantiles = 0.5)
p


p.ldvsco <- p

library(cowplot)
pp <- plot_grid(p.pca, p.hathel, p.ldvsco, labels = "AUTO", nrow = 1)
ggsave(pp, filename = "MapCorrelations.pdf", width = 15, height = 5)


# Generate a table with all correlations and confidence intervals:
# Bootstrap to get Kendall's tau confidence intervals:

require(boot)
require(pcaPP)
boot.cor <- function(data, nbrep) {
  bs <- boot(na.omit(data), statistic = function(original, index, ...) {
    m <- cor.fk(original[index,])
    return(m[1,2])
  }, R = nbrep)
  quantile(bs$t, prob = c(0.025, 0.5, 0.975))
}

get.table <- function(data, variables) {
  n <- length(variables)
  t.cross1 <- numeric(n)
  t.cross2 <- numeric(n)
  t.crossA <- numeric(n)
  p.cross1 <- numeric(n)
  p.cross2 <- numeric(n)
  p.crossA <- numeric(n)
  i.cross1 <- character(n)
  i.cross2 <- character(n)
  i.crossA <- character(n)
  for (i in 1:n) {
    v <- variables[i]
    cat("Computing for ", v, "...\n", sep = "")
    t1 <- cor.test(x = data[,v], y = data[, "3D7x3D1"], method = "kendall")
    t2 <- cor.test(x = data[,v], y = data[, "SW5xSW39"], method = "kendall")
    tA <- cor.test(x = data[,v], y = data[, "AveCrosses"], method = "kendall")
    b1 <- boot.cor(data[, c(v, "3D7x3D1")], nbrep = 10000)
    b2 <- boot.cor(data[, c(v, "SW5xSW39")], nbrep = 10000)
    bA <- boot.cor(data[, c(v, "AveCrosses")], nbrep = 10000)
    t.cross1[i] <- round(t1$estimate, digits = 2)
    t.cross2[i] <- round(t2$estimate, digits = 2)
    t.crossA[i] <- round(tA$estimate, digits = 2)
    p.cross1[i] <- t1$p.value
    p.cross2[i] <- t2$p.value
    p.crossA[i] <- tA$p.value
    i.cross1[i] <- paste("[", round(b1[1], digits = 2), ",", round(b1[3], digits = 2), "]", sep = "")
    i.cross2[i] <- paste("[", round(b2[1], digits = 2), ",", round(b2[3], digits = 2), "]", sep = "")
    i.crossA[i] <- paste("[", round(bA[1], digits = 2), ",", round(bA[3], digits = 2), "]", sep = "")
  }
  return(data.frame(Variable = variables,
                    Tau1 = t.cross1,
                    PValue1 = p.cross1,
                    Interval1 = i.cross1,
                    Tau2 = t.cross2,
                    PValue2 = p.cross2,
                    Interval2 = i.cross2,
                    TauA = t.crossA,
                    PValueA = p.crossA,
                    IntervalA = i.crossA)
        )
}

tbl1 <- get.table(map.co, c("LDhat", "LDhelmet", "AveMethods", "LDhatIntergenic500", "LDhelmetIntergenic500", "LDhatNoSingleton", "LDhat8seq"))
tbl2 <- get.table(map.co.f, c("LDhat", "LDhelmet", "AveMethods", "LDhatIntergenic500", "LDhelmetIntergenic500", "LDhatNoSingleton", "LDhat8seq"))
tbl1$Data <- "Unfiltered"
tbl2$Data <- "Filtered"
tbl <- rbind(tbl1, tbl2)
write.csv(tbl, file = "COCorrelations.csv")

# Figure forpresentation:
require(ggplot2)
require(cowplot)
dat.ld <- data.frame(Chr = map.ldhat.f$Chr, Position = (map.ldhat.f$Loci + map.ldhat.f$Loci2) / 2, Rate = map.ldhat.f$Mean_rho, stringsAsFactors = FALSE)
dat1 <- data.frame(Indv = "3D7x3D1", Chr = map.co$chr, Position = (map.co$start + map.co$end) / 2, Rate = map.co$`3D7x3D1`, stringsAsFactors = FALSE)
dat2 <- data.frame(Indv = "SW5xSW39", Chr = map.co$chr, Position = (map.co$start + map.co$end) / 2, Rate = map.co$`SW5xSW39`, stringsAsFactors = FALSE)
dat.co <- rbind(dat1, dat2)
dat.co$Rate <- dat.co$Rate*1e6/20e3 
p1 <- ggplot(subset(dat.co, Chr == 1), aes(x = Position, y = Rate)) +
  geom_line(aes(color = Indv)) +
  theme_bw() + ylab(expression(paste(r, " (cM/Mb)"))) + xlab("Position (bp)") +
  theme(legend.position = "top",
        plot.background = element_rect(fill = "transparent", color = NA),
        legend.background = element_rect(fill = "transparent", color = NA))
p2 <- ggplot(subset(dat.ld, Chr == 1), aes(x = Position, y = Rate)) +
  geom_line() +
  theme_bw() + ylab(expression(rho == 4%.%Ne%.%r)) + xlab("Position (bp)") +
  theme(plot.background = element_rect(fill = "transparent", color = NA))
p <- plot_grid(p1, p2, ncol = 1)
p

ggsave(p, filename = "RecMapsChr1.pdf", width = 12, height = 6)
# Or LDhat map only:
ggsave(p2, filename = "RecMapsChr1.pdf", width = 12, height = 6)

