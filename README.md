This repository contains the data and scripts necessary to reproduce the analyses in “Fine-Scale Recombination Maps of Fungal Plant Pathogens RevealDynamic Recombination Landscapes and Intragenic Hotspots" by E. H. Stukenbrock and J. Y. Dutheil, published in Genetics March 1, 2018 vol. 208(3):1209-1229 (DOI: https://doi.org/10.1534/genetics.117.300502).

Analyses are grouped per category, with a main R script name SuppMat-XXX.R.

ChangeLog:
==========

- August 2020: updated hotspot analyses, after rerunning LDhot with a background map in kb instead of bp. See Corrigendum.pdf for a detailed description. 
