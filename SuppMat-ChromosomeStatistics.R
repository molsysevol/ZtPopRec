# Analysis of recombination maps: patterns within and between chromosomes
# Created on February 5th, 2016
# by Julien Y. Dutheil and Eva H. Stukenbrock

library(plyr)

# Original recombination maps:
read.rec <- function(file) {
  map <- read.table(file, header = TRUE, stringsAsFactors = FALSE)
  fx <- function(map) {
    map$Loci2 <- c(map$Loci[-1], NA)
    return(map[-nrow(map),])
  }
  dat <- ddply(map, .variables = "Chr", .fun = fx)
  dat$Weight <- dat$Loci2 - dat$Loci
  dat$Loci2 <- NULL
  return(dat)
}

map.zt <- read.rec("ZtRecRate_interval_t0.005.txt.gz")
map.za <- read.rec("ZaRecRate_interval_t0.005.txt.gz")
map.zt.f <- subset(map.zt, (U95-L95) / Mean_rho < 2)
map.za.f <- subset(map.za, (U95-L95) / Mean_rho < 2)
100 * nrow(map.zt.f) / nrow(map.zt) #51% SNPs kept
100 * nrow(map.za.f) / nrow(map.za) #69% SNPs kept

# General statistics:
with(map.zt.f, weighted.mean(Mean_rho, w = Weight)) # mean = 0.0217
with(map.za.f, weighted.mean(Mean_rho, w = Weight)) # mean = 0.0045
# 0.0217/0.0045 ~ 5x
# Only essential chromosomes:
with(subset(map.zt.f, Chr <= 13), weighted.mean(Mean_rho, w = Weight)) # mean = 0.0248
# => very little difference in mean

###
## Average over 10 kb windows
#

# As we will further look at the distance to centromeres, we need to map the Za SNPs to the Zt map (see method section in article)
map.za.mapped <- read.rec("ZaRecRate_interval_t0.005.mapped.txt.gz")
# Warning: weight is here incorrect, should be the one of the original map:
map.za.mapped$Weight <- NULL
map.za.mapped <- merge(map.za.mapped, map.za[, c("Chr", "Loci", "Weight")], by = c("Chr", "Loci"))
map.za.mapped.f <- subset(map.za.mapped, (U95-L95) / Mean_rho < 2)

# Mean rho on dispensable chromosomes:

# Here is the function that slide a window along the two maps.
# Note: this function will also be used for looking at the correlation between the two maps.
slide.window<-function(wsize) {
  l.chr <- list()
  l.pos <- list()
  l.rho.za <- list()
  l.rho.zt <- list()
  l.nb.za <- list()
  l.nb.zt <- list()
  index <- 1
  for (chr in 1:20) {
    cat("\nSliding chromosome ", chr, "...\n", sep = "")
    map.zt.chr <- subset(map.zt.f, Chr == chr)
    map.za.chr <- subset(map.za.mapped.f, chr.target == chr)
    start <- min(map.zt.chr[1, "Loci"], map.za.chr[1, "begin.target"])
    end <- max(map.zt.chr[nrow(map.zt.chr), "Loci"], map.za.chr[nrow(map.za.chr), "begin.target"])
    pos <- start
    pb <- txtProgressBar(1, end, style = 3)
    while (pos + wsize < end) {
      setTxtProgressBar(pb, pos)
      map.zt.chr.w <- subset(map.zt.chr, Loci >= pos & Loci < pos + wsize)
      map.za.chr.w <- subset(map.za.chr, begin.target >= pos & begin.target < pos + wsize)
      mean.zt <- with(map.zt.chr.w, weighted.mean(Mean_rho, Weight))
      mean.za <- with(map.za.chr.w, weighted.mean(Mean_rho, Weight))
      nb.zt <- nrow(map.zt.chr.w)
      nb.za <- nrow(map.za.chr.w)

      l.chr[[index]] <- chr
      l.pos[[index]] <- round(pos + wsize / 2)
      l.rho.zt[[index]] <- mean.zt
      l.rho.za[[index]] <- mean.za
      l.nb.zt[[index]] <- nb.zt
      l.nb.za[[index]] <- nb.za

      pos <- pos + wsize
      index <- index + 1
    }
    setTxtProgressBar(pb, end)
  }
  map.w <- data.frame(
    Chr = unlist(l.chr),
    Pos = unlist(l.pos),
    RhoZt = unlist(l.rho.zt),
    RhoZa = unlist(l.rho.za),
    NbSnpZt = unlist(l.nb.zt),
    NbSnpZa = unlist(l.nb.za))
  return(map.w)
}

map.w10k <- slide.window(1e4)
map.w10k <- subset(map.w10k, NbSnpZt >= 10 & NbSnpZa >= 10)

summary(map.w10k$RhoZt) # mean rho = 0.027180
summary(map.w10k$RhoZa) # mean rho = 0.004936

summary(map.w10k$RhoZt[map.w10k$Chr > 13]) # mean rho = 0.01767

# Now we add centromere information (Zt only):
cen <- read.table(file = "ZtCentromeres.tsv", header = TRUE, stringsAsFactors = FALSE)
cen$Pos <- with(cen, (CenStart + CenStop) / 2)

add.cenStats <- function(wdat) {
  for (i in 1:nrow(wdat)) {
    chr <- wdat[i, "Chr"]
    d <- wdat[i, "Pos"] - cen[cen$Chr == chr, "Pos"]
    wdat[i, "DistToCentromere"] <- abs(d)
    wdat[i, "ChrLength"] <- cen[cen$Chr == chr, "ChrLength"]
    wdat[i, "ChrArmLength"] <- ifelse(d < 0, cen[cen$Chr == chr, "CenStart"], cen[cen$Chr == chr, "ChrLength"] - cen[cen$Chr == chr, "CenStop"])
    wdat[i, "RelDistToCentromere"] <- wdat[i, "DistToCentromere"] / wdat[i, "ChrArmLength"]
  }
  return(wdat)
}

map.w10k <- add.cenStats(map.w10k)

# For later re-use:
save(map.w10k, file = "map_w10k.Rdata")
#load("map_w10k.Rdata")

xzt <- sapply(split(map.w10k$RhoZt, map.w10k$Chr <= 13), median)
xza <- sapply(split(map.w10k$RhoZa, map.w10k$Chr <= 13), median)
sapply(split(map.w10k$RhoZt, map.w10k$Chr <= 13), length)
sapply(split(map.w10k$RhoZa, map.w10k$Chr <= 13), length)
xzt[1] / xzt[2] # 1.9%
xza[1] / xza[2] # 7.8%

###                ###
### Data formating ###
###                ###

library(data.table)

dat <- melt(as.data.table(map.w10k),
            id = c("Chr", "Pos", "DistToCentromere", "ChrLength", "ChrArmLength"),
            measure.vars = list(Rho=c("RhoZa", "RhoZt"), NbSnp=c("NbSnpZa", "NbSnpZt")),
            value.name = c("Rho", "NbSnp"),
            variable.name = "Species")
dat$Species <- c("Za", "Zt")[dat$Species]
dat$Type <- factor(x = dat$Chr <= 13, levels = c(TRUE, FALSE), labels = c("Essential", "Dispensable"))

datsum <- ddply(.data = dat, .variables = c("ChrLength", "Species"), .fun = plyr::summarize,
                medianRho = median(Rho, na.rm = TRUE),
                meanRho = mean(Rho, na.rm = TRUE),
                loRho = quantile(Rho, prob = 0.25, na.rm = TRUE),
                upRho = quantile(Rho, prob = 0.75, na.rm = TRUE),
                Species = unique(Species), Type = unique(Type),
                Chr = unique(Chr))

datsum2 <- ddply(.data = dat, .variables = c("ChrArmLength", "Species"), .fun = plyr::summarize,
                 medianRho = median(Rho, na.rm = TRUE),
                 meanRho = mean(Rho, na.rm = TRUE),
                 loRho = quantile(Rho, prob = 0.25, na.rm = TRUE),
                 upRho = quantile(Rho, prob = 0.75, na.rm = TRUE),
                 Species = unique(Species), Type = unique(Type),
                 Chr = unique(Chr))

###        ###
### Figure ###
###        ###

library(ggplot2)

p1<- ggplot(data = datsum, aes(x = log10(ChrLength), y = log10(medianRho)))
p1 <- p1 + geom_point(aes(pch = Type, color = Species), size = 4)
p1 <- p1 + geom_smooth(aes(color = Species, linetype = Type), method="lm", se = FALSE)
p1 <- p1 + scale_color_manual(values = c(Za = "cornflowerblue", Zt = "cornsilk4"))
p1 <- p1 + theme_bw() + xlab("Log chromosome length") + ylab("Log median rho") + theme(legend.position = "top")

p2<- ggplot(data = datsum2, aes(x = log10(ChrArmLength), y = log10(medianRho)))
p2 <- p2 + geom_point(aes(pch = Type, color = Species), size = 4)
p2 <- p2 + geom_smooth(aes(color = Species, linetype = Type), method="lm", se = FALSE)
p2 <- p2 + scale_color_manual(values = c(Za = "cornflowerblue", Zt = "cornsilk4"))
p2 <- p2 + theme_bw() + xlab("Log chromosome arm length") + ylab("Log median rho") + theme(legend.position = "top")

library(cowplot)
pp <- plot_grid(p1, p2, nrow = 1)
pp

###       ###
### Tests ###
###       ###

cor.test(~medianRho+ChrLength, datsum, subset = Chr %in% 1:13 & Species == "Zt", method="kendall")
#Negative, ***
cor.test(~medianRho+ChrLength, datsum, subset = Chr %in% 1:13 & Species == "Za", method="kendall")
#Negative, ***
cor.test(~medianRho+ChrArmLength, datsum2, subset = Chr %in% 1:13 & Species == "Zt", method="kendall")
#Negative, *
cor.test(~medianRho+ChrArmLength, datsum2, subset = Chr %in% 1:13 & Species == "Za", method="kendall")
#Negative, ***

# Stronger correlations with chr lengths

# Distance to centromere:
cor.test(~RhoZt+DistToCentromere, map.w10k, subset = Chr %in% 1:13, method="kendall")
#Negative, ***
cor.test(~RhoZa+DistToCentromere, map.w10k, subset = Chr %in% 1:13, method="kendall")
#NS
cor.test(~RhoZt+RelDistToCentromere, map.w10k, subset = Chr %in% 1:13, method="kendall")
#Positive, NS
cor.test(~RhoZa+RelDistToCentromere, map.w10k, subset = Chr %in% 1:13, method="kendall")
#Positive ***

###
## Similar analysis, but without windows, all SNPs are directly averaged per chr / chr arm
#

cen <- read.table(file = "ZtCentromeres.tsv", header = TRUE, stringsAsFactors = FALSE)
cen$Pos <- with(cen, (CenStart + CenStop) / 2)

# Compute arm lengths:
a <- data.frame(Chr = paste(cen$Chr, "a", sep = ""), Length = cen$Pos)
b <- data.frame(Chr = paste(cen$Chr, "b", sep = ""), Length = cen$ChrLength - cen$Pos)
cen2 <- rbind(a, b)
row.names(cen2) <- cen2$Chr

# To be efficient, we first compute for each loci if it is before or after the centromere, and then assign lengths:
map.zt.f$CenPos <- cen[map.zt.f$Chr, "Pos"]
map.zt.f[with(map.zt.f, Loci - CenPos) <= 0, "ChrArm"] <- "a"
map.zt.f[with(map.zt.f, Loci - CenPos) > 0, "ChrArm"] <- "b"
map.zt.f[, "ChrArmLength"] <- cen2[with(map.zt.f, paste(Chr, ChrArm, sep = "")), "Length"]
map.zt.f[, "ChrLength"] <- cen[map.zt.f$Chr, "ChrLength"]

map.za.mapped.f$CenPos <- cen[map.za.mapped.f$chr.target, "Pos"]
map.za.mapped.f[with(map.za.mapped.f, begin.target - CenPos) <= 0, "ChrArm"] <- "a"
map.za.mapped.f[with(map.za.mapped.f, begin.target - CenPos) > 0, "ChrArm"] <- "b"
map.za.mapped.f[, "ChrArmLength"] <- cen2[with(map.za.mapped.f, paste(chr.target, ChrArm, sep = "")), "Length"]
map.za.mapped.f[, "ChrLength"] <- cen[map.za.mapped.f$chr.target, "ChrLength"]

library(plyr)
dat.zt1 <- ddply(.data = map.zt.f, .variables = "ChrLength", .fun = summarise, Chr = unique(Chr), MeanRho = weighted.mean(Mean_rho, Weight), Species = "Zt")
dat.za1 <- ddply(.data = map.za.mapped.f, .variables = "ChrLength", .fun = summarise, Chr = unique(chr.target), MeanRho = weighted.mean(Mean_rho, Weight), Species = "Za")
dat1 <- rbind(dat.zt1, dat.za1)
dat1$Type <- factor(dat1$Chr <= 13, levels = c(TRUE, FALSE), labels = c("Essential", "Dispensable"))

dat.zt2 <- ddply(.data = map.zt.f, .variables = "ChrArmLength", .fun = summarise, Chr = unique(Chr), MeanRho = weighted.mean(Mean_rho, Weight), Species = "Zt")
dat.za2 <- ddply(.data = map.za.mapped.f, .variables = "ChrArmLength", .fun = summarise, Chr = unique(chr.target), MeanRho = weighted.mean(Mean_rho, Weight), Species = "Za")
dat2 <- rbind(dat.zt2, dat.za2)
dat2$Type <- factor(dat2$Chr <= 13, levels = c(TRUE, FALSE), labels = c("Essential", "Dispensable"))

###        ###
### Figure ###
###        ###

library(ggplot2)

p1<- ggplot(data = dat1, aes(x = log10(ChrLength), y = log10(MeanRho)))
p1 <- p1 + geom_point(aes(pch = Type, color = Species), size = 4, alpha = 0.7)
p1 <- p1 + geom_smooth(aes(color = Species, linetype = Type), method="lm", se = FALSE)
p1 <- p1 + scale_color_manual(values = c(Za = "cornflowerblue", Zt = "cornsilk4"))
p1 <- p1 + theme_bw() + xlab("Log chromosome length") + ylab("Log mean rho") + theme(legend.position = "top")

p2<- ggplot(data = dat2, aes(x = log10(ChrArmLength), y = log10(MeanRho)))
p2 <- p2 + geom_point(aes(pch = Type, color = Species), size = 4, alpha = 0.7)
p2 <- p2 + geom_smooth(aes(color = Species, linetype = Type), method="lm", se = FALSE)
p2 <- p2 + scale_color_manual(values = c(Za = "cornflowerblue", Zt = "cornsilk4"))
p2 <- p2 + theme_bw() + xlab("Log chromosome arm length") + ylab("Log mean rho") + theme(legend.position = "top")

plot_grid(p1, p2, nrow = 1)

# Nicer representation, dispensable chromosomes apart:
p1 <- ggplot(data = subset(dat1, Type == "Essential"), aes(x = ChrLength/1e6, y = MeanRho))
p1 <- p1 + geom_point(aes(color = Species), size = 4, alpha = 0.7)
p1 <- p1 + geom_smooth(aes(color = Species), method="lm", se = FALSE)
p1 <- p1 + scale_color_manual(values = c(Za = "cornflowerblue", Zt = "cornsilk4"))
p1 <- p1 + theme_bw() + xlab("Chromosome length (Mb)") + ylab(expression(paste("Mean ", rho))) + theme(legend.position = "top") + ylim(c(0, 0.06))

p2 <- ggplot(data = subset(dat2, Type == "Essential"), aes(x = ChrArmLength/1e6, y = MeanRho))
p2 <- p2 + geom_point(aes(color = Species), size = 4, alpha = 0.7)
p2 <- p2 + geom_smooth(aes(color = Species), method="lm", se = FALSE)
p2 <- p2 + scale_color_manual(values = c(Za = "cornflowerblue", Zt = "cornsilk4"))
p2 <- p2 + theme_bw() + xlab("Chromosome arm length (Mb)") + ylab(expression(paste("Mean ", rho))) + theme(legend.position = "top") + ylim(c(0, 0.06))

p3 <- ggplot(data = subset(dat1, Species == "Zt"), aes(x = Type, y = MeanRho))
p3 <- p3 + geom_boxplot(aes(color = Species), varwidth = TRUE) + ylab(expression(paste("Mean ", rho)))
p3 <- p3 + scale_color_manual(values = c(Za = "cornflowerblue", Zt = "cornsilk4"))
p3 <- p3 + theme_bw() + theme(legend.position = "top")

pp <- plot_grid(p1, p2, p3, nrow = 1, rel_widths = c(1,1,0.5), labels = "AUTO")
pp

ggsave(pp, filename = "ChromosomeLengths.pdf", width=11, height=6)

# Simplified version for slides:
pp2 <- plot_grid(p2 + theme(plot.background = element_rect(color = NA, fill = "transparent"),
                            legend.background = element_rect(color = NA, fill = "transparent")),
                 p3 + theme(plot.background = element_rect(color = NA, fill = "transparent"),
                            legend.background = element_rect(color = NA, fill = "transparent")),
                 nrow = 1, rel_widths = c(3,1), labels = "AUTO")
pp2

ggsave(pp2, filename = "ChromosomeLengths2.pdf", width=8, height=6)


###       ###
### Tests ###
###       ###

cor.test(~MeanRho+ChrLength, dat1, subset = Type == "Essential" & Species == "Zt", method="kendall")
#Negative, **
cor.test(~MeanRho+ChrLength, dat1, subset = Type == "Essential" & Species == "Za", method="kendall")
#Negative, ***
cor.test(~MeanRho+ChrArmLength, dat2, subset = Type == "Essential" & Species == "Zt", method="kendall")
#Negative, NS
cor.test(~MeanRho+ChrArmLength, dat2, subset = Type == "Essential" & Species == "Za", method="kendall")
#Negative, **

###                                           ###
### Recombination rate in centromeric regions ###
###                                           ###

# Reread the data:
map.zt <- read.rec("ZtRecRate_interval_t0.005.txt.gz")
map.zt.f <- subset(map.zt, (U95-L95) / Mean_rho < 2)
cen <- read.table(file = "ZtCentromeres.tsv", header = TRUE, stringsAsFactors = FALSE)

# Get repeats information:
library(intervals)
rep <- read.table(file = "Zt09_repeats.gff", stringsAsFactors = FALSE)
rep$Chr <- substring(rep$V1, 5)
tes <- rep[grep(rep$V10, pattern = "Motif:\\(.*\\)n", perl = TRUE, invert = TRUE),]
rep <- rep[grep(rep$V10, pattern = "Motif:\\(.*\\)n", perl = TRUE),]

for (i in 1: nrow(cen)) {
  tmp1 <- subset(map.zt.f, Chr == cen[i, "Chr"])
  tmp2 <- subset(tmp1, Loci >= cen[i, "CenStart"] & Loci < cen[i, "CenStop"])
  tmp3 <- subset(tmp1, Loci < cen[i, "CenStart"] | Loci >= cen[i, "CenStop"])
  cen[i, "Mean_rho"] <- weighted.mean(tmp2$Mean_rho, tmp2$Weight, na.rm = TRUE)
  cen[i, "NbSnp"] <- sum(!is.na(tmp2$Mean_rho))
  cen[i, "Mean_rho.chr"] <- weighted.mean(tmp3$Mean_rho, tmp3$Weight, na.rm = TRUE)
  cen[i, "NbSnp.chr"] <- sum(!is.na(tmp3$Mean_rho))
  tmp4 <- subset(rep, Chr == cen[i, "Chr"])
  tmp5 <- subset(tes, Chr == cen[i, "Chr"])
  ints4 <- Intervals(cbind(tmp4$V4, tmp4$V5))
  ints5 <- Intervals(cbind(tmp5$V4, tmp5$V5))
  int <- Intervals(c(cen[i, "CenStart"], cen[i, "CenStop"]))
  s4 <- sum(size(interval_intersection(int, ints4)))
  s5 <- sum(size(interval_intersection(int, ints5)))
  cen[i, "RepeatDensity"] <- s4 / size(int)
  cen[i, "TEDensity"] <- s5 / size(int)
}
cor.test(~Mean_rho+RepeatDensity, cen, method = "kendall")
cor.test(~Mean_rho+TEDensity, cen, method = "kendall")
boxplot(RepeatDensity~Chr>=13, cen, notch = TRUE)
boxplot(TEDensity~Chr>=13, cen, notch = TRUE)
# No difference

write.csv(cen, file = "RecombinationCentromeres.csv")

plot(Mean_rho~TEDensity, cen);
cor.test(~Mean_rho+TEDensity, cen, method = "kendall")
with(cen, wilcox.test(Mean_rho, Mean_rho.chr, paired = TRUE)) #NS
with(subset(cen, Chr <= 13), wilcox.test(Mean_rho, Mean_rho.chr, paired = TRUE)) #NS

